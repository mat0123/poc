﻿namespace Web.Models
{
    public class BaseModel
    {
        public bool CurrentUserIsAdmin { get; set; }
    }
}
