﻿using System.ComponentModel.DataAnnotations;

namespace Web.Models
{
    public class EditListModel : BaseModel
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public bool ListExpirationEnabled { get; set; }
        public bool Expired { get; set; }
    }
}
