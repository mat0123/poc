﻿using ApplicationLayer.DTO;
using AutoMapper;
using Web.Models;

namespace Web.Mappings
{
    public class ListModelsMappingProfile : Profile
    {
        public ListModelsMappingProfile()
        {
            CreateMap<ListDTO, EditListModel>()
                .ForMember(dest => dest.ListExpirationEnabled, opt => opt.Ignore())
                .ForMember(dest => dest.CurrentUserIsAdmin, opt => opt.Ignore())
                .ReverseMap()
                ;
        }
    }
}
