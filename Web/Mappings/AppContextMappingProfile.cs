﻿using System.Security.Principal;
using ApplicationLayer.Framework.Context;
using AutoMapper;
using Web.Mappings.TypeConverters;

namespace Web.Mappings
{
    public class AppContextMappingProfile : Profile
    {
        public AppContextMappingProfile()
        {
            CreateMap<IPrincipal, CurrentUser>()
                .ConvertUsing<IPrincipalTypeConverter>()
                ;
        }
    }
}
