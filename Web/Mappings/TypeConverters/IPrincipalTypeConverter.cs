﻿using System.Security.Principal;
using ApplicationLayer.Framework.Context;
using AutoMapper;

namespace Web.Mappings.TypeConverters
{
    public class IPrincipalTypeConverter : ITypeConverter<IPrincipal, CurrentUser>
    {
        public CurrentUser Convert(IPrincipal source, CurrentUser destination, ResolutionContext context)
        {
            if (destination == null)
                destination = new CurrentUser();

            destination.Login = source.Identity.Name;
            destination.IsAdmin = source.IsInRole("Admin");
            return destination;
        }
    }
}