﻿using System.Web.Mvc;
using ApplicationLayer.Interfaces.Context;
using Conditions;
using Web.Framework.Filters.ActionFilters;

namespace Web.Controllers
{
    [LoggedInUserFilter]
    public class BaseController : Controller
    {
        private readonly IAppContext _appContext;

        public BaseController(IAppContext appContext)
        {
            Condition.Requires(appContext, "appContext").IsNotNull();

            _appContext = appContext;
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            ViewBag.IsAdmin = _appContext.CurrentUser.IsAdmin;
        }
    }
}