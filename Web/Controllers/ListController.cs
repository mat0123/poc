﻿using ApplicationLayer.Interfaces.Configuration;
using ApplicationLayer.Interfaces.Services;
using AutoMapper;
using Serilog;
using System.Web.Mvc;
using ApplicationLayer.DTO;
using ApplicationLayer.Interfaces.Context;
using Conditions;
using Web.Models;

namespace Web.Controllers
{
    public class ListController : BaseController
    {
        private readonly IListService _listService;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;
        private readonly IConfig _config;
        private readonly IAppContext _appContext;

        public ListController(IListService listService, IMapper mapper, ILogger logger, IConfig config, IAppContext appContext) : base(appContext)
        {
            Condition.Requires(listService, "listService").IsNotNull();
            Condition.Requires(mapper, "mapper").IsNotNull();
            Condition.Requires(logger, "logger").IsNotNull();
            Condition.Requires(config, "config").IsNotNull();
            Condition.Requires(appContext, "appContext").IsNotNull();

            _listService = listService;
            _mapper = mapper;
            _logger = logger;
            _config = config;
            _appContext = appContext;
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            ListDTO listDto = _listService.GetList(id);
            EditListModel model = _mapper.Map<ListDTO, EditListModel>(listDto);

            if (_appContext.CurrentUser.IsAdmin)
                model.CurrentUserIsAdmin = true;

            if (_config.ListExpirationEnabled)
                model.ListExpirationEnabled = true;

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(EditListModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            ListDTO listDto = _mapper.Map<EditListModel, ListDTO>(model);
            _listService.SetList(listDto);

            return View(model);
        }
    }
}