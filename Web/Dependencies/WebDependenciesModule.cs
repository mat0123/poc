﻿using ApplicationLayer.Framework.Configuration;
using ApplicationLayer.Interfaces.Configuration;
using ApplicationLayer.Interfaces.Context;
using Autofac;
using Autofac.Integration.Mvc;
using AutoMapper;
using Serilog;
using Web.Controllers;
using Web.Framework.Context;

namespace Web.Dependencies
{
    public class WebDependenciesModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            builder.RegisterModule<AutofacWebTypesModule>();
            builder.RegisterFilterProvider();

            builder.RegisterInstance(new LoggerConfig().Configure()).As<ILogger>().SingleInstance();
            builder.RegisterInstance(new MapperConfig().Configure()).As<IMapper>().SingleInstance();
            builder.RegisterInstance(new Config()).As<IConfig>().SingleInstance();

            //var config = new ConfigurationConfig().Configure();
            //builder.RegisterInstance(config.GetSection(ApplicationOptions.Key)).As<ApplicationOptions>().SingleInstance();

            builder.RegisterType<AppContext>().As<IAppContext>().InstancePerRequest();
            builder.RegisterType<ListController>().InstancePerRequest();
        }
    }
}