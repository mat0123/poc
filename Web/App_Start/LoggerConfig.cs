﻿using Serilog;

namespace Web
{
    public class LoggerConfig
    {
        public ILogger Configure()
        {
            return new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.RollingFile("log-{Date}.txt")
                .CreateLogger();
        }
    }
}