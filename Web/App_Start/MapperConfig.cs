﻿using ApplicationLayer.Interfaces.Configuration;
using AutoMapper;

namespace Web
{
    public class MapperConfig
    {
        public IMapper Configure()
        {
            return new MapperConfiguration(cfg =>
            {
                cfg.AddProfiles(new[] {
                    typeof(MvcApplication).Assembly, //Web
                    typeof(IConfig).Assembly, //ApplicationLayer
                });
            })
            .CreateMapper();
        }
    }
}