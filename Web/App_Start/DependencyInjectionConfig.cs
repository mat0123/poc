﻿using ApplicationLayer.Interfaces.Configuration;
using Autofac;
using Autofac.Integration.Mvc;
using System.Web.Mvc;

namespace Web
{
    public class DependencyInjectionConfig
    {
        public void Configure()
        {
            var builder = new ContainerBuilder();
            builder.RegisterAssemblyModules(new[]
            {
                typeof(MvcApplication).Assembly, //Web
                typeof(IConfig).Assembly, //ApplicationLayer
            });

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}