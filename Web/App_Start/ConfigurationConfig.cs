﻿using Microsoft.Extensions.Configuration;

namespace Web
{
    public class ConfigurationConfig
    {
        public IConfigurationRoot Configure()
        {
            return new ConfigurationBuilder()
                .AddJsonFile(@"App_Data\config.json")
                .Build();
        }
    }
}