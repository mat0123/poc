﻿using System.Web.Mvc;
using ApplicationLayer.Interfaces.Context;
using Conditions;

namespace Web.Framework.Filters.ActionFilters
{
    public class LoggedInUserFilter : ActionFilterAttribute
    {
        public IAppContext _appContext { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Condition.Requires(_appContext, "_appContext").IsNotNull();

            base.OnActionExecuting(filterContext);

            filterContext.Controller.ViewBag.CurrentUserIsAdmin = _appContext.CurrentUser.IsAdmin;
        }
    }
}