﻿using System.Web;
using ApplicationLayer.Framework.Context;
using ApplicationLayer.Interfaces.Context;
using AutoMapper;
using Conditions;

namespace Web.Framework.Context
{
    public class AppContext : IAppContext
    {
        private readonly HttpContextBase _httpContext;
        private readonly IMapper _mapper;

        public AppContext(HttpContextBase httpContext, IMapper mapper)
        {
            Condition.Requires(httpContext, "httpContext").IsNotNull();
            Condition.Requires(mapper, "mapper").IsNotNull();

            _httpContext = httpContext;
            _mapper = mapper;
        }

        public CurrentUser CurrentUser
        {
            get
            {
                return _mapper.Map<CurrentUser>(_httpContext.User);
            }
        }
    }
}
