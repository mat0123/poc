﻿using System;

namespace DomainClient.DomainObjects
{
    public class ListDetails
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTimeOffset ExpirationDate { get; set; }
    }
}
