﻿using Conditions;
using Common.Exceptions;
using Serilog;
using System;
using DomainClient.DomainObjects;
using DomainClient.Interfaces.Clients;

namespace DomainClient
{
    public class ListClient : IListClient
    {
        private readonly ILogger _logger;

        public ListClient(ILogger logger)
        {
            Condition.Requires(logger, "logger").IsNotNull();

            _logger = logger;
        }

        public ListDetails GetList(int id)
        {
            Condition.Requires(id, "id").IsNotEqualTo(0);

            try
            {
                return new ListDetails
                {
                    Id = id,
                    Name = Guid.NewGuid().ToString(),
                    ExpirationDate = DateTimeOffset.UtcNow.UtcDateTime.AddDays(-1),
                };
            }
            catch (ListNotFoundException)
            {
                _logger.Information("List {Id} not found", id);
                return null;
            }
        }

        public void SetList(ListDetails list)
        {
            Condition.Requires(list.Id, "id");
        }
    }
}
