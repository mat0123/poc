﻿using DomainClient.DomainObjects;

namespace DomainClient.Interfaces.Clients
{
    public interface IListClient
    {
        ListDetails GetList(int id);
        void SetList(ListDetails list);
    }
}
