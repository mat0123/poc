﻿using System;

namespace Common.Exceptions
{
    public class BaseException : Exception
    {
        public Guid CorrelationId { get; protected set; }
    }
}
