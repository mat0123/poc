﻿using System.Reflection;
using Autofac;
using DomainClient.Interfaces.Clients;
using Module = Autofac.Module;

namespace ApplicationLayer.Dependencies
{
    public class ListDependenciesModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(typeof(IListClient).Assembly)
                .Where(t => t.Name.EndsWith("Client"))
                .AsImplementedInterfaces()
                .InstancePerRequest()
                ;

            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                .Where(t => t.Name.EndsWith("Service"))
                .AsImplementedInterfaces()
                .InstancePerRequest()
                ;
        }
    }
}
