﻿using ApplicationLayer.Interfaces.Configuration;

namespace ApplicationLayer.Framework.Configuration
{
    public class Config : IConfig
    {
        public bool ListExpirationEnabled
        {
            get
            {
                return true;
            }
        }
    }
}
