﻿namespace ApplicationLayer.Framework.Context
{
    public class CurrentUser
    {
        public string Login { get; set; }
        public bool IsAdmin { get; set; }
    }
}
