﻿using System;

namespace ApplicationLayer.DTO
{
    public class ListDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Expired { get; set; }
    }
}
