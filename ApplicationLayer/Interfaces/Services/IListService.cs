﻿using ApplicationLayer.DTO;

namespace ApplicationLayer.Interfaces.Services
{
    public interface IListService
    {
        ListDTO GetList(int id);
        void SetList(ListDTO list);
    }
}
