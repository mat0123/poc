﻿using ApplicationLayer.Framework.Context;

namespace ApplicationLayer.Interfaces.Context
{
    public interface IAppContext
    {
        CurrentUser CurrentUser { get; }
    }
}
