﻿namespace ApplicationLayer.Interfaces.Configuration
{
    public interface IConfig
    {
        bool ListExpirationEnabled { get; }
    }
}
