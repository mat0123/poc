﻿using System;
using ApplicationLayer.DTO;
using AutoMapper;
using DomainClient.DomainObjects;

namespace ApplicationLayer.Mappings
{
    public class ListDtoMappingProfile : Profile
    {
        public ListDtoMappingProfile()
        {
            CreateMap<ListDetails, ListDTO>()
                .ForMember(dest => dest.Expired, opt => opt.MapFrom(src => DateTimeOffset.UtcNow.UtcDateTime >= src.ExpirationDate.UtcDateTime))
                .ReverseMap()
                ;
        }
    }
}
