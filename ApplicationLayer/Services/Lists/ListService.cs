﻿using ApplicationLayer.DTO;
using ApplicationLayer.Interfaces.Configuration;
using ApplicationLayer.Interfaces.Context;
using ApplicationLayer.Interfaces.Services;
using AutoMapper;
using Conditions;
using DomainClient.DomainObjects;
using DomainClient.Interfaces.Clients;
using Serilog;

namespace ApplicationLayer.Services.Lists
{
    public class ListService : IListService
    {
        private readonly IListClient _listClient;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;
        private readonly IConfig _config;
        private readonly IAppContext _appContext;

        public ListService(IListClient listClient, IMapper mapper, ILogger logger, IConfig config, IAppContext appContext)
        {
            Condition.Requires(listClient, "listClient").IsNotNull();
            Condition.Requires(mapper, "mapper").IsNotNull();
            Condition.Requires(logger, "logger").IsNotNull();
            Condition.Requires(config, "config").IsNotNull();
            Condition.Requires(appContext, "appContext").IsNotNull();

            _listClient = listClient;
            _mapper = mapper;
            _logger = logger;
            _config = config;
            _appContext = appContext;
        }

        public ListDTO GetList(int id)
        {
            Condition.Requires(id, "id").IsGreaterThan(0);

            ListDetails list = _listClient.GetList(id);
            ListDTO listDto = _mapper.Map<ListDTO>(list);
            return listDto;
        }

        public void SetList(ListDTO listDto)
        {
            Condition.Requires(listDto.Id, "id").IsGreaterThan(0);

            ListDetails list = _mapper.Map<ListDetails>(listDto);
            _listClient.SetList(list);
        }
    }
}
