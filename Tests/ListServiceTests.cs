﻿using ApplicationLayer.DTO;
using ApplicationLayer.Interfaces.Services;
using ApplicationLayer.Services.Lists;
using FakeItEasy;
using FluentAssertions;
using Xunit;

namespace Tests
{
    public class ListServiceTests
    {
        private readonly IListService _target = A.Fake<ListService>();

        [Fact]
        public void GivenAListDomainObject_WhenListDomainObjectIsPastExpirationDate_ThenListApplicationObjectShouldBeExpired()
        {
            //Arrange
            int expected = 1;

            //Act
            ListDTO actual = _target.GetList(1);

            //Assert
            actual.Id.Should().Be(expected);
        }
    }
}
