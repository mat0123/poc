﻿using AutoMapper;
using Web;
using Xunit;

namespace Tests
{
    public class MappingTests
    {
        [Fact]
        public void GivenMapperConfig_WhenMappingSetupComplete_ThenTestIfValid()
        {
            //Arrange
            //Act
            IMapper mapper = new MapperConfig().Configure();

            //Assert
            mapper.ConfigurationProvider.AssertConfigurationIsValid();
        }
    }
}
